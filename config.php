<?php
#true = product, false = local
$mode = true;
if ($mode == true) {
    $path_to = '/var/www/vhosts/fatordigital.com.br/www/preview/loja-rivin';
    $link_to = 'www.fatordigital.com.br/preview/loja-rivin';
} else {
    $path_to = 'C:/laragon/www/rive';
    $link_to = 'rive-fd.dev';
}

// HTTP
define('HTTP_SERVER', 'http://'.$link_to.'/');

// HTTPS
define('HTTPS_SERVER', 'https://'.$link_to.'/');

// DIR
define('DIR_APPLICATION', $path_to . '/catalog/');
define('DIR_SYSTEM', $path_to . '/system/');
define('DIR_IMAGE', $path_to . '/image/');
define('DIR_LANGUAGE', $path_to . '/catalog/language/');
define('DIR_TEMPLATE', $path_to . '/catalog/view/theme/');
define('DIR_CONFIG', $path_to . '/system/config/');
define('DIR_CACHE', $path_to . '/system/storage/cache/');
define('DIR_DOWNLOAD', $path_to . '/system/storage/download/');
define('DIR_LOGS', $path_to . '/system/storage/logs/');
define('DIR_MODIFICATION', $path_to . '/system/storage/modification/');
define('DIR_UPLOAD', $path_to . '/system/storage/upload/');

// DB
if ($mode == true) {
    define('DB_DRIVER', 'mysqli');
    define('DB_HOSTNAME', 'fatordigital.com.br');
    define('DB_USERNAME', 'fd');
    define('DB_PASSWORD', 'fd2016#');
    define('DB_DATABASE', 'rivin');
    define('DB_PORT', '3306');
    define('DB_PREFIX', 'oc_');
} else {
    define('DB_DRIVER', 'mysqli');
    define('DB_HOSTNAME', 'localhost');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', '');
    define('DB_DATABASE', 'rivin');
    define('DB_PORT', '3306');
    define('DB_PREFIX', 'oc_');
}