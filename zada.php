<?php
date_default_timezone_set('America/Sao_Paulo');
include_once('zada_config.php');

$files = glob('xmls/*.xml');
// debug($files);die;

$SalvarProduto = array();
$ExcluirProduto = array();
$SalvarVariacao = array();
$SalvarSubVariacao = array();
$SalvarPreco = array();
$SalvarEstoque = array();
$SalvarDepartamento = array();
$SalvarUsuario = array();
$SalvarEstabelecimento = array();
$ClientesDisponiveis = array();
$ConfirmaRecebimentoClientes = array();
$PedidosDisponiveis = array();
$ConfirmaRecebimentoPedidos = array();

$php_self   = $_SERVER['PHP_SELF'];
$dir        = substr(strrchr($php_self, "/"), 1);
$restante   = str_replace($dir, '', $php_self);

foreach ($files as $file) {

    //EXEMPLOS
    //PRODUTO: 02-10-17-07-15-071506971707.1051
    // $file_salvar_produto = 'xmls/02-10-17-07-15-011506971701.4481.xml';
    // $file_salvar_produto = 'xmls/02-10-17-07-15-301506971730.6024.xml';
    // $file_salvar_produto = 'xmls/02-10-17-07-15-431506971743.8871.xml';
    // $file_salvar_produto = 'xmls/15-12-17-06-06-491513361209.6534.xml';

    // if($file != $file_salvar_produto) continue;

    debug($file);
    print('<hr />');

    $url = file_get_contents($_SERVER['DOCUMENT_ROOT'] . $restante . $file);

    if($url == '') continue;

    $xml = new SimpleXMLElement($url);
    $xml->registerXPathNamespace("soap", "http://www.w3.org/2003/05/soap-envelope");
    $body = $xml->xpath("//S:Body");

    $GLOBALS['logs'] = array();
  
    $insert = array();

    // debug($body);die;

    //new Class
    new Zada();

    foreach ($body as $item) {
        if ($item->SalvarProduto) {

            $current = $item->SalvarProduto;
            $array = json_decode(json_encode($current), true);
            if (has($array, 'produtos', 'Salvar Produto')) {

                if (count($array['produtos'])) {

                    foreach ($current->produtos as $row) {
                        foreach ($row->produto as $item) {

                            $agrupador_site     = (string) $item->agrupador_site;
                            $titulo_variacao    = (string) $item->titulo_variacao;
                            $titulo_subvariacao = (string) $item->titulo_subvariacao;
                            $nome               = (string) $item->descricao;                            
                            $marca              = (string) $item->marca;
                            $desc_marca         = (string) $item->desc_marca;

                            //departamento
                            $departamento_n1    = (string) $item->departamento_n1;
                            $desc_dep_n1        = (string) $item->desc_dep_n1;
                            $departamento_n2    = (string) $item->departamento_n2;
                            $desc_dep_n2        = (string) $item->desc_dep_n2;
                            $departamento_n3    = (string) $item->departamento_n3;
                            $desc_dep_n3        = (string) $item->desc_dep_n3;
                            $departamento_n4    = (string) $item->departamento_n4;
                            $desc_dep_n4        = (string) $item->desc_dep_n4;

                            $listaimagens       = (string) $item->listaimagens;
                            $descricao          = (string) $item->especificacoes_texto;
                            $ativo              = (string) $item->ativo;

                            $imagens            = explode(';', $listaimagens);

                            $insert = array();
                            $insert['oc_product'] = array(
                                'sku'                   => $agrupador_site,
                                'model'                 => $nome,
                                'stock_status_id'       => $ativo == 'S' ? 7 : 5,
                                'zada_agrupador_site'   => $agrupador_site,
                                'zada_dep_n1'           => $departamento_n1,
                                'zada_dep_n2'           => $departamento_n2,
                                'zada_dep_n3'           => $departamento_n3,
                                'zada_dep_n4'           => $departamento_n4,

                                //default
                                'upc'                   => '',
                                'ean'                   => '',
                                'jan'                   => '',
                                'isbn'                  => '',
                                'mpn'                   => '',
                                'location'              => '',
                                'manufacturer_id'       => '0',
                                'tax_class_id'          => '0',
                                'date_available'        => date('Y-m-d'),
                                'date_added'            => date('Y-m-d H:i:s'),
                                'date_modified'         => date('Y-m-d H:i:s'),
                            );

                            $insert['oc_product_description'] = array(
                                'product_id'        => '',
                                'language_id'       => 2,
                                'name'              => $nome,
                                'description'       => $descricao,
                                'tag'               => '',
                                'meta_title'        => $nome,
                                'meta_description'  => substr($descricao, 0, 160),
                                'meta_keyword'      => ''
                            );

                            // $insert['oc_product']['image'] = $imagens[0];

                            if (count($imagens) > 1) {
                                unset($imagens[0]);
                                $insert['oc_product_image'] = array(
                                    'product_id' => '',
                                    'image' => $imagens[1]
                                );
                            }

                            $insert['oc_product_to_layout'] = array(
                                'product_id' => '',
                                'store_id' => 0,
                                'layout_id' => 0
                            );

                            $insert['oc_product_to_store'] = array(
                                'product_id' => '',
                                'store_id' => 0
                            );                            

                            // foreach ($item as $k => $dep) {
                            //     if (strstr($k, 'desc_dep_')) {
                            //         $dep = (string)$dep;
                            //         if (!empty($dep)) {
                            //             $insert['oc_product_to_category'][] = array(
                            //                 'product_id' => '',
                            //                 'category_id' => (string)$dep
                            //             );
                            //         }
                            //     }
                            // }

                            Zada::salvarProduto($insert);
                        }
                    }
                }
            }
        }

        if ($item->SalvarVariacao) {

            $current = $item->SalvarVariacao;

            $array = json_decode(json_encode($current), true);
            if (has($array, 'variacoes', 'Salvar Variacao')) {

                if (count($array['variacoes'])) {

                    foreach ($current->variacoes as $row) {
                        foreach ($row->variacao as $item) {

                            debug($item);

                            $agrupador_site         = (string) $item->agrupador_site;
                            $codigo                 = (string) $item->codigo;
                            $complemento            = (string) $item->complemento;
                            $cor                    = (string) $item->cor;                            
                            $unidade                = (string) $item->unidade;
                            $desc_unidade           = (string) $item->desc_unidade;

                            $ean                    = (string) $item->ean;
                            $lista_imagens          = (string) $item->lista_imagens;
                            $peso_bruto             = (string) $item->peso_bruto;
                            $peso_liquido           = (string) $item->peso_liquido;
                            $cubagem                = (string) $item->cubagem;
                            $especificacoes_texto   = (string) $item->especificacoes_texto;
                            $ativo                  = (string) $item->ativo;

                            $insert = array();
                            $insert['oc_option_value'] = array(
                                // 'option_value_id'   => '',
                                'option_id'         => '13',
                                'image'             => $lista_imagens,
                                'sort_order'        => '0',
                            );

                            $insert['oc_option_value_description'] = array(
                                // 'option_value_id'   => '',
                                'language_id'       => '2',
                                'option_id'         => '13',
                                'name'              => $complemento,
                            );

                            $insert['oc_product_option'] = array(
                                // 'product_option_id'     => '',
                                'product_id'            => '',
                                'option_id'             => '13',
                                'value'                 => $complemento,
                                'required'              => '1',
                                'zada_codigo'           => $codigo,
                                'zada_agrupador_site'   => $agrupador_site,
                            );

                            $insert['oc_product_option_value'] = array(
                                // 'product_option_value_id'   => '',
                                // 'product_option_id'         => '',
                                'product_id'                => '',
                                'option_id'                 => '13',
                                'option_value_id'           => '',
                                'quantity'                  => '0',
                                'subtract'                  => '1',
                                'price'                     => '0.00',
                                'price_prefix'              => '+',
                                'points'                    => '0',
                                'points_prefix'             => '+',
                                'weight'                    => '0.00000000',
                                'weight_prefix'             => '+'
                            );

                            debug($insert);
                            // if($lista_imagens != ""){
                                // debug($insert);
                            // }

                            
                            Zada::salvarVariacao($insert);
                        }
                    }

                }
            }
        }

        if ($item->SalvarPreco) {

            $current = $item->SalvarPreco;

            $array = json_decode(json_encode($current), true);

            if (has($array, 'produtos', 'Salvar Preco')) {

                if (count($array['produtos']) > 0) {
                    foreach ($current->produtos as $departamento) {
                        foreach ($departamento->produto as $item) {

                            $codigo                     = (string) $item->codigo;
                            $codpreco                   = (string) $item->codpreco;
                            $preco_original             = (string) $item->preco_original;
                            $preco_atual                = (string) $item->preco_atual;
                            $preco_agendamento_data     = (string) $item->preco_agendamento_data;
                            $preco_agendamento_preco    = (string) $item->preco_agendamento_preco;
                            $preco_promocao_inicio      = (string) $item->preco_promocao_inicio;
                            $preco_promocao_final       = (string) $item->preco_promocao_final;
                            $preco_promocao_preco       = (string) $item->preco_promocao_preco;

                            $insert = array();
                            $insert['oc_product_option'] = array(
                                'zada_codigo'       => $codigo
                            );

                            $insert['oc_product_option_value'] = array(
                                'price'          => str_replace(',', '.', $preco_atual)
                            );

                            // debug($insert);

                            Zada::salvarPreco($insert);

                        }
                    }
                }
            }

        }

        if ($item->SalvarEstoque) {
            $current = $item->SalvarEstoque;

            $array = json_decode(json_encode($current), true);

            if (has($array, 'produtos', 'Salvar Estoque')) {

                if (count($array['produtos']) > 0) {
                    foreach ($current->produtos as $departamento) {
                        foreach ($departamento->produto as $item) {

                            $codigo                 = (int) $item->codigo;
                            $estoque_disponivel     = (int) $item->estoque_disponivel;

                            $insert = array();
                            $insert['oc_product_option'] = array(
                                'zada_codigo'       => $codigo
                            );

                            $insert['oc_product_option_value'] = array(
                                'quantity'          => $estoque_disponivel
                            );

                            Zada::salvarEstoque($insert);
                        }
                    }
                }
            }
        }

        if ($item->SalvarDepartamento) {

            $current = $item->SalvarDepartamento;

            // debug($current);die;

            $array = json_decode(json_encode($current), true);

            if (has($array, 'departamentos', 'Salvar Departamento')) {

                if (count($array['departamentos']) > 0) {
                    foreach ($current->departamentos as $departamento) {
                        foreach ($departamento->departamento as $item) {

                            $nivel           = (string) $item->nivel;
                            $codigo          = (string) $item->codigo;
                            $descricao       = (string) $item->descricao;
                            $ativo           = (string) $item->ativo;                            

                            $insert = array();
                            $insert['oc_category'] = array(
                                // 'category_id'       => '',
                                // 'image'             => '',
                                'parent_id'         => '0',
                                'top'               => '1',
                                'column'            => '1',
                                'sort_order'        => '0',
                                'status'            => $ativo == 'S' ? 1 : 0,
                                'date_added'        => date('Y-m-d H:i:s'),
                                'date_modified'     => date('Y-m-d H:i:s'),
                                'zada_nivel'        => $nivel,
                                'zada_codigo'       => $codigo,
                                'zada_ref'          => $codigo,
                            );

                            $insert['oc_category_description'] = array(
                                // 'category_id'       => '',
                                'language_id'       => '2',
                                'name'              => $descricao,
                                'description'       => '',
                                'meta_title'        => $descricao,
                                'meta_description'  => '',
                                'meta_keyword'      => '',
                            );

                            $insert['oc_category_path'] = array(
                                // 'category_id'       => '',
                                'path_id'           => '',
                                'level'             => '1',
                            );

                            $insert['oc_category_to_layout'] = array(
                                // 'category_id'       => '',
                                'store_id'          => '0',
                                'layout_id'         => '0',
                            );

                            $insert['oc_category_to_store'] = array(
                                // 'category_id'       => '',
                                'store_id'          => '0',
                            );

                            // debug($insert);
                            // die;


                            if($nivel == 1){
                                Zada::salvarDepartamento($insert);
                            }else{
                                $conn = Database::connection();

                                $sql = $conn->prepare("SELECT * FROM " . DB_PREFIX . "category WHERE parent_id = :parent_id AND zada_codigo IS NOT NULL");
                                $sql->execute(array(
                                    ':parent_id'   => 0
                                ));
                                $categories = $sql->fetchAll(PDO::FETCH_OBJ);

                                // debug($categories);
                                // die;

                                if($categories){
                                    foreach ($categories as $key => $category) {
                                        $insert['oc_category']['parent_id'] = $category->category_id;
                                        $insert['oc_category']['status']    = 0;
                                        $insert['oc_category']['top']       = 0;
                                        $insert['oc_category']['zada_ref']  = $category->zada_ref.'-'.$insert['oc_category']['zada_codigo'];

                                        $insert['oc_category_path']['level']    = '0';
                                        $insert['oc_category_path']['path_id']  = $insert['oc_category']['parent_id'];

                                        // debug($insert);
                                        Zada::salvarDepartamento($insert);
                                    }
                                }

                                // die;
                            }                            
                        }
                    }
                } 
            }
        }

        if (isset($item['SalvarSubVariacao'])) {

        }

        if (isset($item['SalvarEstabelecimento'])) {

        }

        if (isset($item['ClientesDisponiveis'])) {

        }

        if (isset($item['ConfirmaRecebimentoClientes'])) {

        }

        if (isset($item['PedidosDisponiveis'])) {

        }

        if (isset($item['ConfirmaRecebimentoPedidos'])) {

        }

        if (isset($item['ExcluirProduto'])) {

        }
    }

}

// echo '<pre>';
// die(print_r($SalvarProduto));