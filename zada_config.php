<?php 

if (is_file('config.php')) {
  require_once('config.php');
}

class Database {
  protected static $db;

  private function __construct() {
    try {
      self::$db = new PDO('mysql:host='.DB_HOSTNAME.';dbname='.DB_DATABASE.'', DB_USERNAME, DB_PASSWORD);
      self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      // self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
      self::$db->exec('SET NAMES utf8');
    } catch (PDOException $e) {
        print "Error Database: " . $e->getMessage();
        die();
    }
  }

  public static function connection() {
    if (!self::$db) 
      new Database();
      return self::$db;
  }

  public static function getHeaders() {
    if(function_exists('apache_request_headers')) {
      return apache_request_headers();
    }
    $headers = array();
    $keys = preg_grep('{^HTTP_}i', array_keys($_SERVER));
    foreach($keys as $val) {
      $key = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($val, 5)))));
      $headers[$key] = $_SERVER[$val];
    }
    return $headers;
  }
}

class Zada {

    protected static $conn;
    protected static $ftpConnection;
    protected static $catalogFile;
    protected static $root;

    public function __construct(){
        self::$conn = Database::connection();

        self::$ftpConnection = ftp_connect('54.245.47.110');
        ftp_login(self::$ftpConnection, 'zada', 'zada2017#');
        ftp_pasv(self::$ftpConnection, true);

        self::$catalogFile  = 'image/catalog/products/';
        self::$root         = "C:\\laragon\\www\\fatordigital\\rivin\\";
    }

    public function salvarProduto($data){
        if (!self::$conn) 
            new Zada();

        if(isset($data['oc_product'])){
            if($data['oc_product']['zada_agrupador_site'] != ""){

                //BEGIN: product

                //cria o array de fields e values
                $fields = array();
                $values = array();
                foreach ($data['oc_product'] as $key => $value) {
                    $fields[$key]       = $value;
                    $values[':' . $key] = $value;
                }
                
                //verifica se o produto já existe
                $sql = self::$conn->prepare("SELECT product_id FROM " . DB_PREFIX . "product WHERE zada_agrupador_site = :zada_agrupador_site");
                $sql->execute(array(
                    ':zada_agrupador_site' => $data['oc_product']['zada_agrupador_site']
                ));
                $product = $sql->fetch(PDO::FETCH_OBJ);
                if($product){
                    $data['oc_product']['product_id'] = $product->product_id;
                    unset($fields['date_added']);
                }               

                //não tem product_id? é insert
                if(!isset($data['oc_product']['product_id'])){
                    $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "product 
                                                        (" . implode(',', array_keys($fields)) . ")
                                                    VALUES
                                                        (" . implode(',', array_keys($values)) . ")
                                                ");    
                }else{
                    //estruturo a query do update
                    $update = '';
                    foreach ($fields as $key => $field) {
                        $update .= $key . ' = :' . $key . ', ';
                    }
                    $update = substr($update, 0, strlen($update)-2);

                    //prepare
                    $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "product SET
                                                       " . $update . "
                                                    WHERE
                                                        product_id = :product_id");

                    //crio o index product_id
                    $fields[':product_id'] = $data['oc_product']['product_id'];
                }

                //execute query
                if($sql->execute($fields)){

                    //get product_id
                    if(!isset($data['oc_product']['product_id'])){
                        $data['oc_product']['product_id'] = self::$conn->lastInsertId();
                    }

                    if($data['oc_product']['product_id'] > 0){

                        //BEGIN: product_description
                        $data['oc_product_description']['product_id'] = $data['oc_product']['product_id'];

                        //cria o array de fields e values
                        $fields = array();
                        $values = array();
                        foreach ($data['oc_product_description'] as $key => $value) {
                            $fields[$key]       = $value;
                            $values[':' . $key] = $value;
                        }

                        //verifica se a descrição do produto já existe
                        $sql = self::$conn->prepare("SELECT product_id FROM " . DB_PREFIX . "product_description WHERE product_id = :product_id");
                        $sql->execute(array(
                            ':product_id' => $data['oc_product']['product_id']
                        ));
                        $product_description = $sql->fetch(PDO::FETCH_OBJ);
                        if($product_description){
                            //estruturo a query do update
                            $update = '';
                            foreach ($fields as $key => $field) {
                                $update .= $key . ' = :' . $key . ', ';
                            }
                            $update = substr($update, 0, strlen($update)-2);

                            //prepare
                            $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "product_description SET
                                                               " . $update . "
                                                            WHERE
                                                                product_id = :product_id");
                        }else{
                            $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "product_description 
                                                                (" . implode(',', array_keys($fields)) . ")
                                                            VALUES
                                                                (" . implode(',', array_keys($values)) . ")
                                                        ");    
                        }

                        $sql->execute($fields);


                        //BEGIN: oc_product_to_layout
                        $data['oc_product_to_layout']['product_id'] = $data['oc_product']['product_id'];

                        //cria o array de fields e values
                        $fields = array();
                        $values = array();
                        foreach ($data['oc_product_to_layout'] as $key => $value) {
                            $fields[$key]       = $value;
                            $values[':' . $key] = $value;
                        }

                        //verifica se a relação com layout do produto já existe
                        $sql = self::$conn->prepare("SELECT product_id FROM " . DB_PREFIX . "product_to_layout WHERE product_id = :product_id");
                        $sql->execute(array(
                            ':product_id' => $data['oc_product']['product_id']
                        ));
                        $product_to_layout = $sql->fetch(PDO::FETCH_OBJ);
                        if($product_to_layout){
                            // //estruturo a query do update
                            // $update = '';
                            // foreach ($fields as $key => $field) {
                            //     $update .= $key . ' = :' . $key . ', ';
                            // }
                            // $update = substr($update, 0, strlen($update)-2);

                            // //prepare
                            // $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "product_to_layout SET
                            //                                    " . $update . "
                            //                                 WHERE
                            //                                     product_id = :product_id");
                            // $sql->execute($fields);
                        }else{
                            $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "product_to_layout 
                                                                (" . implode(',', array_keys($fields)) . ")
                                                            VALUES
                                                                (" . implode(',', array_keys($values)) . ")
                                                        ");
                            $sql->execute($fields);
                        }

                        //BEGIN: oc_product_to_store
                        $data['oc_product_to_store']['product_id'] = $data['oc_product']['product_id'];

                        //cria o array de fields e values
                        $fields = array();
                        $values = array();
                        foreach ($data['oc_product_to_store'] as $key => $value) {
                            $fields[$key]       = $value;
                            $values[':' . $key] = $value;
                        }

                        //verifica se a relação com loja do produto já existe
                        $sql = self::$conn->prepare("SELECT product_id FROM " . DB_PREFIX . "product_to_store WHERE product_id = :product_id");
                        $sql->execute(array(
                            ':product_id' => $data['oc_product']['product_id']
                        ));
                        $product_to_store = $sql->fetch(PDO::FETCH_OBJ);
                        if($product_to_store){
                            // //estruturo a query do update
                            // $update = '';
                            // foreach ($fields as $key => $field) {
                            //     $update .= $key . ' = :' . $key . ', ';
                            // }
                            // $update = substr($update, 0, strlen($update)-2);

                            // //prepare
                            // $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "product_to_store SET
                            //                                    " . $update . "
                            //                                 WHERE
                            //                                     product_id = :product_id");
                            // $sql->execute($fields);
                        }else{
                            $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "product_to_store 
                                                                (" . implode(',', array_keys($fields)) . ")
                                                            VALUES
                                                                (" . implode(',', array_keys($values)) . ")
                                                        ");    
                            $sql->execute($fields);
                        }
                    }
                }
            }
        }
    }

    public function salvarVariacao($data){
        if (!self::$conn) 
            new Zada();

        if(isset($data['oc_product_option'])){
            if($data['oc_product_option']['zada_agrupador_site'] != ""){

                if(empty($data['oc_option_value_description']['name'])) return false;

                //BEGIN: product
                //verifica se o produto já existe
                $sql = self::$conn->prepare("SELECT product_id FROM " . DB_PREFIX . "product WHERE zada_agrupador_site = :zada_agrupador_site");
                $sql->execute(array(
                    ':zada_agrupador_site' => $data['oc_product_option']['zada_agrupador_site']
                ));
                $oc_product = $sql->fetch(PDO::FETCH_OBJ);
                if($oc_product){
                    $product_id = $oc_product->product_id;

                    $data['oc_product_option']['product_id']        = $product_id;
                    $data['oc_product_option_value']['product_id']  = $product_id;
                }else{
                    return false;
                }

                //images
                 if(!empty($data['oc_option_value']['image'])){
                    $imagens = explode(';', $data['oc_option_value']['image']);

                    foreach ($imagens as $key => $value) {
                        $image = self::downloadImage($value);

                        if($image){

                            $image = str_replace('image/', '', $image);

                            $sql = self::$conn->prepare("SELECT image FROM " . DB_PREFIX . "product WHERE product_id = :product_id");
                            $sql->execute(array(
                                ':product_id'      => $product_id
                            ));
                            $product = $sql->fetch(PDO::FETCH_OBJ);
                            if(empty($product->image) || is_null($product->image)){
                                $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "product SET image = :image
                                                        WHERE
                                                            product_id = :product_id");

                                $sql->execute(array(
                                    ':image'        => $image,
                                    ':product_id'   => $product_id
                                ));
                            }else{
                                $sql = self::$conn->prepare("SELECT count(image) as total FROM " . DB_PREFIX . "product_image WHERE image = :image AND product_id = :product_id");
                                $sql->execute(array(
                                    ':product_id'       => $product_id,
                                    ':image'            => $image,
                                ));
                                $product = $sql->fetch(PDO::FETCH_OBJ);
                                if($product->total == 0){

                                    $a = array(
                                        'product_id' => $product_id,
                                        'image'      => $image,
                                        'sort_order' => 0,
                                    );

                                    $fields = array();
                                    $values = array();
                                    foreach ($a as $key => $value) {
                                        $fields[$key]       = $value;
                                        $values[':' . $key] = $value;
                                    }

                                    $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "product_image 
                                                        (" . implode(',', array_keys($fields)) . ")
                                                    VALUES
                                                        (" . implode(',', array_keys($values)) . ")
                                                ");
                                    $sql->execute($fields);
                                }
                            }    
                        }
                    }
                }
                //images


                //BEGIN: oc_option_value

                //verifica se o option_value_description já existe
                $sql = self::$conn->prepare("SELECT option_value_id FROM " . DB_PREFIX . "option_value_description WHERE name = :name AND option_id = :option_id");
                $sql->execute(array(
                    ':name'      => $data['oc_option_value_description']['name'],
                    ':option_id' => $data['oc_option_value_description']['option_id']
                ));
                $option_value_description = $sql->fetch(PDO::FETCH_OBJ);
                if($option_value_description){
                    $data['oc_option_value_description']['option_value_id'] = $option_value_description->option_value_id;
                }               

                //não tem option_value_id? é insert
                if(!isset($data['oc_option_value_description']['option_value_id'])){

                    //cria o array de fields e values
                    $fields = array();
                    $values = array();
                    foreach ($data['oc_option_value'] as $key => $value) {
                        $fields[$key]       = $value;
                        $values[':' . $key] = $value;
                    }

                    $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "option_value 
                                                        (" . implode(',', array_keys($fields)) . ")
                                                    VALUES
                                                        (" . implode(',', array_keys($values)) . ")
                                                ");

                    if($sql->execute($fields)){
                        $option_value_id = self::$conn->lastInsertId();
                        $data['oc_option_value']['option_value_id']             = $option_value_id;
                        $data['oc_option_value_description']['option_value_id'] = $option_value_id;
                        $data['oc_product_option_value']['option_value_id'] = $option_value_id;
                    }

                }else{
                    $data['oc_option_value']['option_value_id']         = $data['oc_option_value_description']['option_value_id'];
                    $data['oc_product_option_value']['option_value_id'] = $data['oc_option_value_description']['option_value_id'];
                }

                // debug($data);
                // die;

                //BEGIN: oc_option_value_description
                if(isset($data['oc_option_value_description']['option_value_id']) && $data['oc_option_value_description']['option_value_id'] > 0){
                    //cria o array de fields e values
                    $fields = array();
                    $values = array();
                    foreach ($data['oc_option_value_description'] as $key => $value) {
                        $fields[$key]       = $value;
                        $values[':' . $key] = $value;
                    }

                    //verifica se o option_value_description já existe
                    $sql = self::$conn->prepare("SELECT option_value_id FROM " . DB_PREFIX . "option_value_description WHERE option_value_id = :option_value_id AND option_id = :option_id");
                    $sql->execute(array(
                        ':option_value_id'  => $data['oc_option_value_description']['option_value_id'],
                        ':option_id'        => $data['oc_option_value_description']['option_id']
                    ));
                    $option_value_description = $sql->fetch(PDO::FETCH_OBJ);
                    if($option_value_description){
                        
                        //estruturo a query do update
                        $update = '';
                        foreach ($fields as $key => $field) {
                            $update .= $key . ' = :' . $key . ', ';
                        }
                        $update = substr($update, 0, strlen($update)-2);

                        //prepare
                        $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "option_value_description SET
                                                           " . $update . "
                                                        WHERE
                                                            option_value_id = :option_value_id");

                    }else{
                        $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "option_value_description 
                                                        (" . implode(',', array_keys($fields)) . ")
                                                    VALUES
                                                        (" . implode(',', array_keys($values)) . ")
                                                ");
                    }

                    if($sql->execute($fields)){

                        //BEGIN: oc_product_option
                        //verifica se o product_option já existe
                        $sql = self::$conn->prepare("SELECT product_option_id, product_option_value_id FROM " . DB_PREFIX . "product_option_value WHERE product_id = :product_id AND option_value_id = :option_value_id");
                        $sql->execute(array(
                            ':product_id'       => $data['oc_product_option_value']['product_id'],
                            ':option_value_id'  => $data['oc_product_option_value']['option_value_id']
                        ));
                        $product_option_value = $sql->fetch(PDO::FETCH_OBJ);
                        if($product_option_value){
                            $product_option_id = $product_option_value->product_option_id;
                            $data['oc_product_option']['product_option_id']         = $product_option_id;
                            $data['oc_product_option_value']['product_option_id']   = $product_option_id;

                            $product_option_value_id = $product_option_value->product_option_value_id;
                            $data['oc_product_option_value']['product_option_value_id']   = $product_option_value_id;
                        }

                        //não tem option_value_id? é insert
                        if(!isset($data['oc_product_option']['product_option_id'])){

                            //cria o array de fields e values
                            $fields = array();
                            $values = array();
                            foreach ($data['oc_product_option'] as $key => $value) {
                                $fields[$key]       = $value;
                                $values[':' . $key] = $value;
                            }

                            $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "product_option 
                                                                (" . implode(',', array_keys($fields)) . ")
                                                            VALUES
                                                                (" . implode(',', array_keys($values)) . ")
                                                        ");

                            if($sql->execute($fields)){
                                $product_option_id = self::$conn->lastInsertId();
                                $data['oc_product_option']['product_option_id']         = $product_option_id;
                                $data['oc_product_option_value']['product_option_id']   = $product_option_id;
                            }
                        }

                        //BEGIN: oc_product_option_value

                        //cria o array de fields e values
                        $fields = array();
                        $values = array();
                        foreach ($data['oc_product_option_value'] as $key => $value) {
                            $fields[$key]       = $value;
                            $values[':' . $key] = $value;
                        }

                        
                        if(isset($data['oc_product_option_value']['product_option_value_id'])){
                            
                            //estruturo a query do update
                            $update = '';
                            foreach ($fields as $key => $field) {
                                $update .= $key . ' = :' . $key . ', ';
                            }
                            $update = substr($update, 0, strlen($update)-2);

                            //prepare
                            $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "product_option_value SET
                                                               " . $update . "
                                                            WHERE
                                                                product_option_value_id = :product_option_value_id");

                        }else{
                            $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "product_option_value 
                                                            (" . implode(',', array_keys($fields)) . ")
                                                        VALUES
                                                            (" . implode(',', array_keys($values)) . ")
                                                    ");
                        }

                        $sql->execute($fields);
                    }else{
                        die('aiaiai');
                    }
                }
            }
        }
    }

    public function salvarDepartamento($data){
        if (!self::$conn) 
            new Zada();

        if(isset($data['oc_category_description'])){
            if($data['oc_category_description']['name'] != ""){

                //BEGIN: category

                //cria o array de fields e values
                $fields = array();
                $values = array();
                foreach ($data['oc_category'] as $key => $value) {
                    $fields[$key]       = $value;
                    $values[':' . $key] = $value;
                }
                
                //verifica se o produto já existe
                // $sql = self::$conn->prepare("SELECT category_id FROM " . DB_PREFIX . "category WHERE zada_nivel = :zada_nivel AND zada_codigo = :zada_codigo");
                // $sql->execute(array(
                //     ':zada_nivel'   => $data['oc_category']['zada_nivel'],
                //     ':zada_codigo'  => $data['oc_category']['zada_codigo']
                // ));
                $sql = self::$conn->prepare("SELECT category_id FROM " . DB_PREFIX . "category WHERE zada_ref = :zada_ref");
                $sql->execute(array(
                    ':zada_ref'   => $data['oc_category']['zada_ref']
                ));
                $category = $sql->fetch(PDO::FETCH_OBJ);
                if($category){
                    $data['oc_category']['category_id'] = $category->category_id;
                    unset($fields['date_added']);
                }               

                //não tem product_id? é insert
                if(!isset($data['oc_category']['category_id'])){
                    $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "category 
                                                        (" . implode(', ' . DB_PREFIX . 'category.', array_keys($fields)) . ")
                                                    VALUES
                                                        (" . implode(',', array_keys($values)) . ")
                                                ");    
                }else{
                    //estruturo a query do update
                    $update = '';
                    foreach ($fields as $key => $field) {
                        $update .= DB_PREFIX . "category." . $key . ' = :' . $key . ', ';
                    }
                    $update = substr($update, 0, strlen($update)-2);

                    //prepare
                    $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "category SET
                                                       " . $update . "
                                                    WHERE
                                                        category_id = :category_id");

                    //crio o index product_id
                    $fields[':category_id'] = $data['oc_category']['category_id'];
                }

                //execute query
                if($sql->execute($fields)){

                    //get product_id
                    if(!isset($data['oc_category']['category_id'])){
                        $data['oc_category']['category_id'] = self::$conn->lastInsertId();
                    }

                    if($data['oc_category']['category_id'] > 0){

                        //BEGIN: category_description
                        $data['oc_category_description']['category_id'] = $data['oc_category']['category_id'];

                        //cria o array de fields e values
                        $fields = array();
                        $values = array();
                        foreach ($data['oc_category_description'] as $key => $value) {
                            $fields[$key]       = $value;
                            $values[':' . $key] = $value;
                        }

                        //verifica se a descrição da categoria já existe
                        $sql = self::$conn->prepare("SELECT category_id FROM " . DB_PREFIX . "category_description WHERE category_id = :category_id");
                        $sql->execute(array(
                            ':category_id' => $data['oc_category']['category_id']
                        ));
                        $category_description = $sql->fetch(PDO::FETCH_OBJ);
                        if($category_description){
                            //estruturo a query do update
                            $update = '';
                            foreach ($fields as $key => $field) {
                                $update .= $key . ' = :' . $key . ', ';
                            }
                            $update = substr($update, 0, strlen($update)-2);

                            //prepare
                            $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "category_description SET
                                                               " . $update . "
                                                            WHERE
                                                                category_id = :category_id");
                        }else{
                            $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "category_description 
                                                                (" . implode(',', array_keys($fields)) . ")
                                                            VALUES
                                                                (" . implode(',', array_keys($values)) . ")
                                                        ");    
                        }

                        $sql->execute($fields);

                        //BEGIN: oc_category_to_layout
                        $data['oc_category_to_layout']['category_id'] = $data['oc_category']['category_id'];

                        //cria o array de fields e values
                        $fields = array();
                        $values = array();
                        foreach ($data['oc_category_to_layout'] as $key => $value) {
                            $fields[$key]       = $value;
                            $values[':' . $key] = $value;
                        }

                        //verifica se a relação com layout da categoria já existe
                        $sql = self::$conn->prepare("SELECT category_id FROM " . DB_PREFIX . "category_to_layout WHERE category_id = :category_id");
                        $sql->execute(array(
                            ':category_id' => $data['oc_category']['category_id']
                        ));
                        $category_to_layout = $sql->fetch(PDO::FETCH_OBJ);

                        if($category_to_layout){
                            // //estruturo a query do update
                            // $update = '';
                            // foreach ($fields as $key => $field) {
                            //     $update .= $key . ' = :' . $key . ', ';
                            // }
                            // $update = substr($update, 0, strlen($update)-2);

                            // //prepare
                            // $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "category_to_layout SET
                            //                                    " . $update . "
                            //                                 WHERE
                            //                                     product_id = :product_id");
                            // $sql->execute($fields);
                        }else{
                            $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "category_to_layout 
                                                                (" . implode(',', array_keys($fields)) . ")
                                                            VALUES
                                                                (" . implode(',', array_keys($values)) . ")
                                                        ");  
                            $sql->execute($fields);  
                        }

                        

                        //BEGIN: oc_category_to_store
                        $data['oc_category_to_store']['category_id'] = $data['oc_category']['category_id'];

                        //cria o array de fields e values
                        $fields = array();
                        $values = array();
                        foreach ($data['oc_category_to_store'] as $key => $value) {
                            $fields[$key]       = $value;
                            $values[':' . $key] = $value;
                        }

                        //verifica se a relação com loja do categoria já existe
                        $sql = self::$conn->prepare("SELECT category_id FROM " . DB_PREFIX . "category_to_store WHERE category_id = :category_id");
                        $sql->execute(array(
                            ':category_id' => $data['oc_category']['category_id']
                        ));
                        $category_to_store = $sql->fetch(PDO::FETCH_OBJ);
                        if($category_to_store){
                            // //estruturo a query do update
                            // $update = '';
                            // foreach ($fields as $key => $field) {
                            //     $update .= $key . ' = :' . $key . ', ';
                            // }
                            // $update = substr($update, 0, strlen($update)-2);

                            // //prepare
                            // $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "category_to_store SET
                            //                                    " . $update . "
                            //                                 WHERE
                            //                                     product_id = :product_id");
                            // $sql->execute($fields); 
                        }else{
                            $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "category_to_store 
                                                                (" . implode(',', array_keys($fields)) . ")
                                                            VALUES
                                                                (" . implode(',', array_keys($values)) . ")
                                                        ");
                            $sql->execute($fields); 
                        }

                        //BEGIN: oc_category_path
                        $data['oc_category_path']['category_id'] = $data['oc_category']['category_id'];
                        if($data['oc_category']['parent_id'] == 0){
                            $data['oc_category_path']['path_id'] = $data['oc_category']['category_id'];
                        }

                        //cria o array de fields e values
                        $fields = array();
                        $values = array();
                        foreach ($data['oc_category_path'] as $key => $value) {
                            $fields[$key]       = $value;
                            $values[':' . $key] = $value;
                        }

                        //verifica se a relação com loja do categoria já existe
                        $sql = self::$conn->prepare("SELECT category_id FROM " . DB_PREFIX . "category_path WHERE category_id = :category_id AND path_id = :path_id");
                        $sql->execute(array(
                            ':category_id'  => $data['oc_category']['category_id'],
                            ':path_id'      => $data['oc_category_path']['path_id']
                        ));
                        $category_to_store = $sql->fetch(PDO::FETCH_OBJ);
                        if($category_to_store){
                            //estruturo a query do update
                            // $update = '';
                            // foreach ($fields as $key => $field) {
                            //     $update .= $key . ' = :' . $key . ', ';
                            // }
                            // $update = substr($update, 0, strlen($update)-2);

                            // //prepare
                            // $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "category_path SET
                            //                                    " . $update . "
                            //                                 WHERE
                            //                                     category_id = :category_id");
                            // $sql->execute($fields); 
                        }else{
                            $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "category_path 
                                                                (" . implode(',', array_keys($fields)) . ")
                                                            VALUES
                                                                (" . implode(',', array_keys($values)) . ")
                                                        ");
                            $sql->execute($fields); 
                        }

                        //BEGIN: oc_category_path level 1
                        if($data['oc_category']['parent_id'] > 0){
                            $data['oc_category_path']['path_id'] = $data['oc_category']['category_id'];
                            $data['oc_category_path']['level']   = '1';

                            //cria o array de fields e values
                            $fields = array();
                            $values = array();
                            foreach ($data['oc_category_path'] as $key => $value) {
                                $fields[$key]       = $value;
                                $values[':' . $key] = $value;
                            }

                            //verifica se a relação com loja do categoria já existe
                            $sql = self::$conn->prepare("SELECT category_id FROM " . DB_PREFIX . "category_path WHERE category_id = :category_id AND level = :level");
                            $sql->execute(array(
                                ':category_id'  => $data['oc_category']['category_id'],
                                ':level'        => $data['oc_category_path']['level']
                            ));
                            $category_to_store = $sql->fetch(PDO::FETCH_OBJ);
                            if($category_to_store){
                                //estruturo a query do update
                                // $update = '';
                                // foreach ($fields as $key => $field) {
                                //     $update .= $key . ' = :' . $key . ', ';
                                // }
                                // $update = substr($update, 0, strlen($update)-2);

                                // //prepare
                                // $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "category_path SET
                                //                                    " . $update . "
                                //                                 WHERE
                                //                                     category_id = :category_id");
                                // $sql->execute($fields); 
                            }else{
                                $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "category_path 
                                                                    (" . implode(',', array_keys($fields)) . ")
                                                                VALUES
                                                                    (" . implode(',', array_keys($values)) . ")
                                                            ");
                                $sql->execute($fields); 
                            }
                        }

                        //validação dos vinculos de cateogira
                        if($data['oc_category']['zada_nivel'] == 1){
                            $sql = self::$conn->prepare("SELECT * FROM " . DB_PREFIX . "product WHERE zada_dep_n" . $data['oc_category']['zada_nivel'] . " = :zada_codigo");
                            $sql->execute(array(
                                ':zada_codigo'  => $data['oc_category']['zada_codigo']
                            ));    
                        }elseif($data['oc_category']['zada_nivel'] == 2){
                            $explode = explode('-', $data['oc_category']['zada_ref']);

                            $sql = self::$conn->prepare("SELECT * FROM " . DB_PREFIX . "product WHERE zada_dep_n" . $data['oc_category']['zada_nivel'] . " = :zada_codigo AND zada_dep_n" . ($data['oc_category']['zada_nivel']-1) . " = :zada_codigo_anterior");

                            $sql->execute(array(
                                ':zada_codigo'           => $data['oc_category']['zada_codigo'],
                                ':zada_codigo_anterior'  => $explode[0]
                            ));    
                        }
                        
                        $products = $sql->fetchAll(PDO::FETCH_OBJ);
                        if($products){

                            // print_r($products);

                            foreach ($products as $key => $item) {
                                
                                //verifica se a relação com loja do produto já existe
                                $sql = self::$conn->prepare("SELECT product_id FROM " . DB_PREFIX . "product_to_category WHERE product_id = :product_id AND category_id = :category_id");
                                $sql->execute(array(
                                    ':product_id'   => $item->product_id,
                                    ':category_id'  => $data['oc_category']['category_id'],
                                ));
                                $product_to_category = $sql->fetch(PDO::FETCH_OBJ);
                                if($product_to_category){
                                    // //estruturo a query do update
                                    // $update = '';
                                    // foreach ($fields as $key => $field) {
                                    //     $update .= $key . ' = :' . $key . ', ';
                                    // }
                                    // $update = substr($update, 0, strlen($update)-2);

                                    // //prepare
                                    // $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "product_to_category SET
                                    //                                    " . $update . "
                                    //                                 WHERE
                                    //                                     product_id = :product_id");
                                    // $sql->execute($fields);
                                }else{
                                    $sql = self::$conn->prepare("INSERT INTO " . DB_PREFIX . "product_to_category 
                                                                        (product_id, category_id)
                                                                    VALUES
                                                                        (:product_id, :category_id)
                                                                ");    
                                    $sql->execute(array(
                                        ':product_id'   => $item->product_id,
                                        ':category_id'  => $data['oc_category']['category_id'],
                                    ));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function salvarEstoque($data){
        if (!self::$conn) 
            new Zada();

        // debug($data);die;

        if(isset($data['oc_product_option'])){
            if($data['oc_product_option']['zada_codigo'] != ""){

                //verifica se o produto já existe
                $sql = self::$conn->prepare("SELECT product_option_id, product_id FROM " . DB_PREFIX . "product_option WHERE zada_codigo = :zada_codigo");
                $sql->execute(array(
                    ':zada_codigo' => $data['oc_product_option']['zada_codigo']
                ));
                $product_option = $sql->fetch(PDO::FETCH_OBJ);

                if($product_option){
                    $product_option_id = $product_option->product_option_id;

                    $data['oc_product_option_value']['product_option_id']  = $product_option_id;

                     //cria o array de fields e values
                    $fields = array();
                    $values = array();
                    foreach ($data['oc_product_option_value'] as $key => $value) {
                        $fields[$key]       = $value;
                        $values[':' . $key] = $value;
                    }

                    // debug($data);die('a');
                    
                    if(isset($data['oc_product_option_value']['product_option_id'])){
                        
                        //estruturo a query do update
                        $update = '';
                        foreach ($fields as $key => $field) {
                            $update .= $key . ' = :' . $key . ', ';
                        }
                        $update = substr($update, 0, strlen($update)-2);

                        //prepare
                        $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "product_option_value SET
                                                           " . $update . "
                                                        WHERE
                                                            product_option_id = :product_option_id");

                        $sql->execute($fields);

                        //BEFIN: count estoque
                            $sql = self::$conn->prepare("SELECT SUM(quantity) as total FROM " . DB_PREFIX . "product_option_value WHERE product_id = :product_id");
                            $sql->execute(array(
                                ':product_id' => $product_option->product_id
                            ));
                            $product_stock = $sql->fetch(PDO::FETCH_OBJ);
                            if($product_stock){
                                $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "product SET quantity = :quantity, status = :status
                                                        WHERE
                                                            product_id = :product_id");

                                $sql->execute(array(
                                    ':quantity'     => $product_stock->total,
                                    ':product_id'   => $product_option->product_id,
                                    ':status'       => ($product_stock->total > 0) ? '1' : '0'
                                ));
                            }
                        //END: count estoque

                        // $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "product_option_value SET
                        //                                    " . $update . "
                        //                                 WHERE
                        //                                     product_option_id = :product_option_id");

                        // $sql->execute($fields);

                    }else{
                        return false;
                    }
                }else{
                    return false;
                }

            }
        }
    }

    public function salvarPreco($data){
        if (!self::$conn) 
            new Zada();

        // debug($data);die;

        if(isset($data['oc_product_option'])){
            if($data['oc_product_option']['zada_codigo'] != ""){

                //verifica se o produto já existe
                $sql = self::$conn->prepare("SELECT product_option_id, product_id FROM " . DB_PREFIX . "product_option WHERE zada_codigo = :zada_codigo");
                $sql->execute(array(
                    ':zada_codigo' => $data['oc_product_option']['zada_codigo']
                ));
                $product_option = $sql->fetch(PDO::FETCH_OBJ);
                if($product_option){
                    $product_option_id = $product_option->product_option_id;

                    $data['oc_product_option_value']['product_option_id']  = $product_option_id;

                     //cria o array de fields e values
                    $fields = array();
                    $values = array();

                    $price = $data['oc_product_option_value']['price'];
                    $data['oc_product_option_value']['price'] = '0.00';
                    foreach ($data['oc_product_option_value'] as $key => $value) {
                        $fields[$key]       = $value;
                        $values[':' . $key] = $value;
                    }
                    
                    if(isset($data['oc_product_option_value']['product_option_id'])){
                        
                        //estruturo a query do update
                        $update = '';
                        foreach ($fields as $key => $field) {
                            $update .= $key . ' = :' . $key . ', ';
                        }
                        $update = substr($update, 0, strlen($update)-2);

                        //prepare
                        $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "product_option_value SET
                                                           " . $update . "
                                                        WHERE
                                                            product_option_id = :product_option_id");

                        $sql->execute($fields);

                        $sql = self::$conn->prepare("UPDATE " . DB_PREFIX . "product SET price = :price
                                                        WHERE
                                                            product_id = :product_id");

                        $sql->execute(array(
                            ':price'        => $price,
                            ':product_id'   => $product_option->product_id
                        ));
                        
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }

            }
        }
    }

    // $image = $this->downloadImage($image);
    static function downloadImage($image){

        $ftpConnection = self::$ftpConnection;
        $catalog_file  = self::$catalogFile;
        $root          = self::$root; 

        // var_dump($image);die;
        $ftpFiles = ftp_nlist($ftpConnection, '/www');

        $local_file = 'tmp_image/' . $image;
        $server_file = '/www/' . $image;
        $catalog_file .= $image;

        if (in_array($server_file, $ftpFiles) && !file_exists($local_file)) {
            if (ftp_get($ftpConnection, $local_file, $server_file, FTP_BINARY, 0)) {
                rename($root.$local_file, $root.$catalog_file);
                return $catalog_file;
            } else {
                return false;
            }
        } else {
            return $catalog_file;
        }

    }
}




function debug($data){
    print('<pre>');
    print_r($data);
}

function has($data, $key, $ref){
    if (!isset($data[$key])) {
        throw new Exception("{$ref}: Não encontrado a INDEX: {$key}");
    }
    if (empty($data[$key])) {
        throw new Exception("{$ref}: {$key} está vazia");
    }
    return true;
}

function has_log($data, $key, $ref){
    if (!isset($data[$key])) {
        $GLOBALS['logs'][] = "{$ref}: {$key} não foi encontrado. Dados passado " . json_encode($data);
        return false;
    } else if (empty($data[$key])) {
        $GLOBALS['logs'][] = "{$ref}: {$key} está vazio. Dados passado" . json_encode($data);
        return false;
    }
    return true;
}


function coinToBco($get_valor){
    if (strstr($get_valor, ',')) {
        $source = array(
            '.',
            ','
        );
        $replace = array(
            '',
            '.'
        );
        $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
    } else
        $valor = $get_valor;

    return $valor; //retorna o valor formatado para gravar no banco
}

// function create_query($insert, $mysql){

//     foreach ($insert as $table => $table_fields) {

//         $count = '';
//         $sql_exists = '';
//         $fields = implode('`,`', array_keys($table_fields));
//         $values = implode('","', array_values($table_fields));
//         $where = '';
//         $set = '';
//         foreach ($table_fields as $table_field => $table_field_value) {
//             if (strstr($table_field, '_id')) {
//                 $where .= sprintf('`%s` = "%s" AND ', $table_field, $table_field_value);
//                 $set .= sprintf('`%s` = "%s", ', $table_field, $table_field_value);
//             }
//         }
//         $set = substr($set, 0, strlen($set) - 2);
//         $where = substr($where, 0, strlen($where) - 4);

//         $sql_insert = sprintf('INSERT INTO %s (`%s`) VALUES ("%s")', $table, $fields, $values);
//         $result = $mysql->query($sql_insert);
//     }

// }

// function update($mysql, $table, $fields_values = array(), $wheres = array()){
//     $set = '';
//     foreach ($fields_values as $table_field => $table_field_value) {
//         $set .= sprintf('`%s` = "%s", ', $table_field, $table_field_value);
//     }

//     $set = substr($set, 0, strlen($set) - 2);

//     $where = '';
//     foreach ($wheres as $table_field => $table_field_value) {
//         $where .= sprintf('`%s` = "%s" AND ', $table_field, $table_field_value);
//     }
//     $where = substr($where, 0, strlen($where) - 4);

//     $sql_update = sprintf('UPDATE `%s` SET %s WHERE %s', $table, $set, $where);
//     $mysql->query($sql_update) or die(mysqli_error($mysql));
// }

// function insert($mysql, $table, $fields_values = array()){
//     $fields = implode('`,`', array_keys($fields_values));
//     $values = implode('","', array_values($fields_values));

//     $sql_insert = sprintf('INSERT INTO %s (`%s`) VALUES ("%s")', $table, $fields, $values);
//     echo '<pre>';
//     $mysql->query($sql_insert) or die(print_r($fields_values));

//     return $mysql->insert_id;
// }

// function exists($mysql, $table, $fields_values = array()){

//     $where = '';
//     foreach ($fields_values as $table_field => $table_field_value) {
//         $where .= sprintf('`%s` = "%s" AND ', $table_field, $table_field_value);
//     }
//     $where = substr($where, 0, strlen($where) - 4);

//     $sql_select = sprintf('SELECT COUNT(*) AS Contador FROM `%s` WHERE %s', $table, $where);
//     $result = $mysql->query($sql_select);
//     $result = $result->fetch_object();

//     return $result->Contador > 0;

// }

// function select($mysql, $table, $fields_values = array()){
//     $where = '';
//     foreach ($fields_values as $table_field => $table_field_value) {
//         $where .= sprintf('`%s` = "%s" AND ', $table_field, $table_field_value);
//     }
//     $where = substr($where, 0, strlen($where) - 4);

//     $sql_select = sprintf('SELECT * FROM `%s` WHERE %s', $table, $where);

//     $result = $mysql->query($sql_select);
//     $result = $result->fetch_object();

//     return $result;
// }

?>